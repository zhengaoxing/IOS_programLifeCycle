//
//  AppDelegate.h
//  IOS_programLifeCycle
//
//  Created by Maculish Ting on 15/4/28.
//  Copyright (c) 2015年 LYD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

